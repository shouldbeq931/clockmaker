# clockmaker

Taken from the clockmaker script used by the [Stratum-1-Microserver HOWTO](https://www.ntpsec.org/white-papers/stratum-1-microserver-howto/#_build_and_configure_ntpsec).

Modified to use native systemd instead of a System V init script.

to set to stationary mode compile gpsControl.c (from https://ava.upuaut.net/?p=951).

`gcc -o gpsControl gpsControl.c`

and set stationary mode.

`sudo ./gpsControl -s -d /dev/ttyAMA0 = Stationary Mode`
